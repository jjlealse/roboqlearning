﻿using UnityEngine;
using System.Collections;

public class LocalizationScript : MonoBehaviour {

	private int stateCurrent;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		RaycastHit hit;
		if (Physics.Raycast(transform.position, -Vector3.up, out hit, 100)) {
			stateCurrent = int.Parse(hit.transform.gameObject.tag);
			Debug.Log(hit.transform.gameObject.tag);
		}
		Debug.DrawRay(transform.position, -Vector3.up);
	}

	public int GetStateCurrent() {
		return this.stateCurrent;
	}
}
