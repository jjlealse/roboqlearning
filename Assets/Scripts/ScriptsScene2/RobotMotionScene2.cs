﻿using UnityEngine;
using System.Collections;

public class RobotMotionScene2 : MonoBehaviour {

	public float speed;
	private Animator animator;
	private LocalizationScript localizationScript;
	private QlearningScene2Script qLearningScript;

	const int stateA = 0;
	const int stateB = 1;
	const int stateC = 2;
	const int stateD = 3;
	const int stateE = 4;
	const int stateF = 5;
	const int stateG = 6;
	const int stateH = 7;
	const int stateI = 8;
	const int stateJ = 9;
	const int stateL = 10;

	private int estadoAtual;

	private Vector3 rotateFrente = new Vector3(0f, 0f, 0f);
	private Vector3 rotateAtras = new Vector3(180f, 0f, 0f);
	private Vector3 rotateCima = new Vector3(-90f, 0f, 0f);
	private Vector3 rotateBaixo = new Vector3(90f, 0f, 0f);

	private DirectionEnum directionCurrent;

	// Use this for initialization
	void Start () {
		estadoAtual = -1;
		animator = GetComponent<Animator>();

		localizationScript = GetComponent<LocalizationScript> ();
		qLearningScript = Camera.main.GetComponent<QlearningScene2Script> ();
	}
	
	// Update is called once per frame
	void Update () {

		MoveRobot ();
		RotateRobot();

	}

	public void SetPosition(Vector3 position) {
		transform.position = position;
	}

	private void MoveRobot() {
		float currentSpeed = 0.0f;
		int stateFrom = localizationScript.GetStateCurrent();
		int stateTo = qLearningScript.getPolicy (stateFrom);

		switch (stateFrom) {
		case stateA :
			currentSpeed = speed;
			if (stateTo == stateB) {
				transform.position = new Vector3 (
					transform.position.x,
					transform.position.y,
					transform.position.z + currentSpeed * Time.deltaTime);
			} else if (stateTo == stateE) {
				transform.position = new Vector3 (
					transform.position.x + currentSpeed * Time.deltaTime,
					transform.position.y,
					transform.position.z);
			}
			break;
		case stateB:
			currentSpeed = speed;
			if (stateTo == stateC) {
				transform.position = new Vector3 (
					transform.position.x,
					transform.position.y,
					transform.position.z + currentSpeed * Time.deltaTime);
			} else if (stateTo == stateA) {
				transform.position = new Vector3 (
					transform.position.x,
					transform.position.y,
					transform.position.z - currentSpeed * Time.deltaTime);
			}
			break;
		case stateC:
			currentSpeed = speed;
			if (stateTo == stateD) {
				transform.position = new Vector3 (
					transform.position.x,
					transform.position.y,
					transform.position.z + currentSpeed * Time.deltaTime);
			} else if (stateTo == stateB) {
				transform.position = new Vector3 (
					transform.position.x,
					transform.position.y,
					transform.position.z - currentSpeed * Time.deltaTime);
			} else if (stateTo == stateF) {
				transform.position = new Vector3 (
					transform.position.x + currentSpeed * Time.deltaTime,
					transform.position.y,
					transform.position.z);
			}

			break;

		case stateD:
			// Objetivo
			currentSpeed = 0;
			break;
		case stateE:
			currentSpeed = speed;
			if (stateTo == stateA) {
				transform.position = new Vector3 (
					transform.position.x - currentSpeed * Time.deltaTime,
					transform.position.y,
					transform.position.z);
			} else if (stateTo == stateH) {
				transform.position = new Vector3 (
					transform.position.x + currentSpeed * Time.deltaTime,
					transform.position.y,
					transform.position.z);
			} 
			break;
		case stateF:
			currentSpeed = speed;
			if (stateTo == stateC) {
				transform.position = new Vector3 (
					transform.position.x - currentSpeed * Time.deltaTime,
					transform.position.y,
					transform.position.z);
			} else if (stateTo == stateG) {
				transform.position = new Vector3 (
					transform.position.x,
					transform.position.y,
					transform.position.z + currentSpeed * Time.deltaTime);
			} else if (stateTo == stateJ) {
				transform.position = new Vector3 (
					transform.position.x + currentSpeed * Time.deltaTime,
					transform.position.y,
					transform.position.z);
			}
			break;
		case stateG:
			currentSpeed = speed;
			if (stateTo == stateF) {
				transform.position = new Vector3 (
					transform.position.x,
					transform.position.y,
					transform.position.z - currentSpeed * Time.deltaTime);
			} else if (stateTo == stateD) {
				transform.position = new Vector3 (
					transform.position.x - currentSpeed * Time.deltaTime,
					transform.position.y,
					transform.position.z);
			} else if (stateTo == stateL) {
				transform.position = new Vector3 (
					transform.position.x + currentSpeed * Time.deltaTime,
					transform.position.y,
					transform.position.z);
			}

			break;
		case stateH:
			currentSpeed = speed;
			if (stateTo == stateE) {
				transform.position = new Vector3 (
					transform.position.x - currentSpeed * Time.deltaTime,
					transform.position.y,
					transform.position.z);
			} else if (stateTo == stateI) {
				transform.position = new Vector3 (
					transform.position.x,
					transform.position.y,
					transform.position.z  + currentSpeed * Time.deltaTime);
			}
			break;

		case stateI:
			currentSpeed = speed;
			if (stateTo == stateH) {
				transform.position = new Vector3 (
					transform.position.x,
					transform.position.y,
					transform.position.z - currentSpeed * Time.deltaTime);
			} else if (stateTo == stateJ) {
				transform.position = new Vector3 (
					transform.position.x,
					transform.position.y,
					transform.position.z + currentSpeed * Time.deltaTime);
			}
			break;

		case stateJ:
			currentSpeed = speed;
			if (stateTo == stateI) {
				transform.position = new Vector3 (
					transform.position.x,
					transform.position.y,
					transform.position.z - currentSpeed * Time.deltaTime);
			} else if (stateTo == stateF) {
				transform.position = new Vector3 (
					transform.position.x - currentSpeed * Time.deltaTime,
					transform.position.y,
					transform.position.z);
			} else if (stateTo == stateL) {
				transform.position = new Vector3 (
					transform.position.x,
					transform.position.y,
					transform.position.z + currentSpeed * Time.deltaTime);
			}
			break;

		case stateL:
			currentSpeed = speed;
			if (stateTo == stateJ) {
				transform.position = new Vector3 (
					transform.position.x,
					transform.position.y,
					transform.position.z - currentSpeed * Time.deltaTime);
			} else if (stateTo == stateG) {
				transform.position = new Vector3 (
					transform.position.x - currentSpeed * Time.deltaTime,
					transform.position.y,
					transform.position.z);
			}

			break;
		}

		animator.SetFloat ("Speed", currentSpeed);
	}

	private void RotateRobot() {
		int stateFrom = localizationScript.GetStateCurrent();
		int stateTo = qLearningScript.getPolicy (stateFrom);
		
		switch (stateFrom) {
		case stateA :
			if (stateTo == stateB) {
				if (directionCurrent != DirectionEnum.Right) {
					transform.RotateAround(transform.position, Vector3.up, 90f);
					directionCurrent = DirectionEnum.Right;
				}
			} else if (stateTo == stateE) {
				if (directionCurrent != DirectionEnum.Down) {
					transform.RotateAround(transform.position, Vector3.up, 0f);
					directionCurrent = DirectionEnum.Down;
				}
			}
			break;
		case stateB:
			if (stateTo == stateC) {
				if (directionCurrent != DirectionEnum.Right) {
					transform.RotateAround(transform.position, Vector3.up, 90f);
					directionCurrent = DirectionEnum.Right;
				}
			} else if (stateTo == stateA) {
				if (directionCurrent != DirectionEnum.Left) {
					transform.RotateAround(transform.position, Vector3.up, -90f);
					directionCurrent = DirectionEnum.Left;
				}
			}
			break;
		case stateC:
			if (stateTo == stateB) {
				if (directionCurrent != DirectionEnum.Left) {
					transform.RotateAround(transform.position, Vector3.up, -90f);
					directionCurrent = DirectionEnum.Left;
				}
			} else if (stateTo == stateD) {
				if (directionCurrent != DirectionEnum.Right) {
					transform.RotateAround(transform.position, Vector3.up, 90f);
					directionCurrent = DirectionEnum.Right;
				}
			} else if (stateTo == stateF) {
				if (directionCurrent != DirectionEnum.Down) {
					transform.RotateAround(transform.position, Vector3.up, 0f);
					directionCurrent = DirectionEnum.Down;
				}
			}
			break;
			
		case stateD:
			// Objetivo
			break;
		case stateE:
			if (stateTo == stateA) {
				if (directionCurrent != DirectionEnum.Up) {
					transform.RotateAround(transform.position, Vector3.up, 180f);
					directionCurrent = DirectionEnum.Up;
				}
			} else if (stateTo == stateH) {
				if (directionCurrent != DirectionEnum.Down) {
					transform.RotateAround(transform.position, Vector3.up, 0f);
					directionCurrent = DirectionEnum.Down;
				}
			} 
			break;
		case stateF:
			if (stateTo == stateC) {
				if (directionCurrent != DirectionEnum.Up) {
					transform.RotateAround(transform.position, Vector3.up, 180f);
					directionCurrent = DirectionEnum.Up;
				}
			} else if (stateTo == stateG) {
				if (directionCurrent != DirectionEnum.Right) {
					transform.RotateAround(transform.position, Vector3.up, 90f);
					directionCurrent = DirectionEnum.Right;
				}
			} else if (stateTo == stateJ) {
				if (directionCurrent != DirectionEnum.Down) {
					transform.RotateAround(transform.position, Vector3.up, 0f);
					directionCurrent = DirectionEnum.Down;
				}
			}
			break;
		case stateG:
			if (stateTo == stateD) {
				if (directionCurrent != DirectionEnum.Up) {
					transform.RotateAround(transform.position, Vector3.up, 180f);
					directionCurrent = DirectionEnum.Up;
				}
			} else if (stateTo == stateF) {
				if (directionCurrent != DirectionEnum.Left) {
					transform.RotateAround(transform.position, Vector3.up, -90f);
					directionCurrent = DirectionEnum.Left;
				}
			} else if (stateTo == stateL) {
				if (directionCurrent != DirectionEnum.Down) {
					transform.RotateAround(transform.position, Vector3.up, 0f);
					directionCurrent = DirectionEnum.Down;
				}
			}

			break;
		case stateH:
			if (stateTo == stateE) {
				if (directionCurrent != DirectionEnum.Up) {
					transform.RotateAround(transform.position, Vector3.up, 180f);
					directionCurrent = DirectionEnum.Up;
				}
			} else if (stateTo == stateI) {
				if (directionCurrent != DirectionEnum.Right) {
					transform.RotateAround(transform.position, Vector3.up, 90f);
					directionCurrent = DirectionEnum.Right;
				}
			}
			break;
		case stateI:
			if (stateTo == stateH) {
				if (directionCurrent != DirectionEnum.Left) {
					transform.RotateAround(transform.position, Vector3.up, -90f);
					directionCurrent = DirectionEnum.Left;
				}
			} else if (stateTo == stateJ) {
				if (directionCurrent != DirectionEnum.Right) {
					transform.RotateAround(transform.position, Vector3.up, 90f);
					directionCurrent = DirectionEnum.Right;
				}
			}
			break;
		case stateJ:
			if (stateTo == stateF) {
				if (directionCurrent != DirectionEnum.Up) {
					transform.RotateAround(transform.position, Vector3.up, 180f);
					directionCurrent = DirectionEnum.Up;
				}
			} else if (stateTo == stateI) {
				if (directionCurrent != DirectionEnum.Left) {
					transform.RotateAround(transform.position, Vector3.up, -90f);
					directionCurrent = DirectionEnum.Left;
				}
			} else if (stateTo == stateL) {
				if (directionCurrent != DirectionEnum.Right) {
					transform.RotateAround(transform.position, Vector3.up, 90f);
					directionCurrent = DirectionEnum.Right;
				}
			}

			break;
		case stateL:
			if (stateTo == stateG) {
				if (directionCurrent != DirectionEnum.Up) {
					transform.RotateAround(transform.position, Vector3.up, 180f);
					directionCurrent = DirectionEnum.Up;
				}
			} else if (stateTo == stateJ) {
				if (directionCurrent != DirectionEnum.Left) {
					transform.RotateAround(transform.position, Vector3.up, -90f);
					directionCurrent = DirectionEnum.Left;
				}
			}
			break;
		}

	}
}
