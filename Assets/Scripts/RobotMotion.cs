﻿using UnityEngine;
using System.Collections;

public class RobotMotion : MonoBehaviour {

	public float speed;
	private Animator animator;
	private LocalizationScript localizationScript;
	private QlearningScript qLearningScript;

	const int stateA = 0;
	const int stateB = 1;
	const int stateC = 2;
	const int stateD = 3;
	const int stateE = 4;
	const int stateF = 5;

	private int estadoAtual;

	private Vector3 rotateFrente = new Vector3(0f, 0f, 0f);
	private Vector3 rotateAtras = new Vector3(180f, 0f, 0f);
	private Vector3 rotateCima = new Vector3(-90f, 0f, 0f);
	private Vector3 rotateBaixo = new Vector3(90f, 0f, 0f);

	private DirectionEnum directionCurrent;

	// Use this for initialization
	void Start () {
		estadoAtual = -1;
		animator = GetComponent<Animator>();

		localizationScript = GetComponent<LocalizationScript> ();
		qLearningScript = Camera.main.GetComponent<QlearningScript> ();
	}
	
	// Update is called once per frame
	void Update () {

		MoveRobot ();
		RotateRobot();

	}

	public void SetPosition(Vector3 position) {
		transform.position = position;
	}

	private void MoveRobot() {
		float currentSpeed = 0.0f;
		int stateFrom = localizationScript.GetStateCurrent();
		int stateTo = qLearningScript.getPolicy (stateFrom);

		switch (stateFrom) {
		case stateA :
			currentSpeed = speed;
			if (stateTo == stateB) {
				transform.position = new Vector3 (
					transform.position.x,
					transform.position.y,
					transform.position.z + currentSpeed * Time.deltaTime);
			} else if (stateTo == stateD) {
				transform.position = new Vector3 (
					transform.position.x + currentSpeed * Time.deltaTime,
					transform.position.y,
					transform.position.z);
			}
			break;
		case stateB:
			currentSpeed = speed;
			if (stateTo == stateC) {
				transform.position = new Vector3 (
					transform.position.x,
					transform.position.y,
					transform.position.z + currentSpeed * Time.deltaTime);
			} else if (stateTo == stateE) {
				transform.position = new Vector3 (
					transform.position.x + currentSpeed * Time.deltaTime,
					transform.position.y,
					transform.position.z);
			} else if (stateTo == stateA) {
				transform.position = new Vector3 (
					transform.position.x,
					transform.position.y,
					transform.position.z - currentSpeed * Time.deltaTime);
			}
			break;
		case stateC:
			// Objetivo
			currentSpeed = 0;
			break;

		case stateD:
			currentSpeed = speed;
			if (stateTo == stateA) {
				transform.position = new Vector3 (
					transform.position.x - currentSpeed * Time.deltaTime,
					transform.position.y,
					transform.position.z);
			} else if (stateTo == stateE) {
				transform.position = new Vector3 (
					transform.position.x,
					transform.position.y,
					transform.position.z  + currentSpeed * Time.deltaTime);
			}
			break;
		case stateE:
			currentSpeed = speed;
			if (stateTo == stateB) {
				transform.position = new Vector3 (
					transform.position.x - currentSpeed * Time.deltaTime,
					transform.position.y,
					transform.position.z);
			} else if (stateTo == stateF) {
				transform.position = new Vector3 (
					transform.position.x,
					transform.position.y,
					transform.position.z  + currentSpeed * Time.deltaTime);
			} else if (stateTo == stateD) {
				transform.position = new Vector3 (
					transform.position.x,
					transform.position.y,
					transform.position.z  - currentSpeed * Time.deltaTime);
			}
			break;
		case stateF:
			currentSpeed = speed;
			if (stateTo == stateC) {
				transform.position = new Vector3 (
					transform.position.x - currentSpeed * Time.deltaTime,
					transform.position.y,
					transform.position.z);
			} else if (stateTo == stateE) {
				transform.position = new Vector3 (
					transform.position.x,
					transform.position.y,
					transform.position.z  - currentSpeed * Time.deltaTime);
			}
			break;
		}

		animator.SetFloat ("Speed", currentSpeed);
	}

	private void RotateRobot() {
		int stateFrom = localizationScript.GetStateCurrent();
		int stateTo = qLearningScript.getPolicy (stateFrom);
		
		switch (stateFrom) {
		case stateA :
			if (stateTo == stateB) {
				if (directionCurrent != DirectionEnum.Right) {
					transform.RotateAround(transform.position, Vector3.up, 90f);
					directionCurrent = DirectionEnum.Right;
				}
			} else if (stateTo == stateD) {
				if (directionCurrent != DirectionEnum.Down) {
					transform.forward = Vector3.RotateTowards(transform.forward, rotateBaixo, 
					                                          90 * Time.deltaTime, 0.0f);
					directionCurrent = DirectionEnum.Down;
				}
			}
			break;
		case stateB:
			if (stateTo == stateC) {
				transform.forward = Vector3.RotateTowards(transform.forward, rotateFrente, 
				                                          90 * Time.deltaTime, 0.0f);
			} else if (stateTo == stateE) {
				transform.forward = Vector3.RotateTowards(transform.forward, rotateBaixo, 
				                                          90 * Time.deltaTime, 0.0f);
			} else if (stateTo == stateA) {
				transform.forward = Vector3.RotateTowards(transform.forward, rotateAtras, 
				                                          90 * Time.deltaTime, 0.0f);
			}
			break;
		case stateC:
			// Objetivo

			break;
			
		case stateD:
			if (stateTo == stateA) {
				if (directionCurrent != DirectionEnum.Up) {
					transform.RotateAround(transform.position, Vector3.up, -90f);
					directionCurrent = DirectionEnum.Up;
				}
			} else if (stateTo == stateE) {
				transform.forward = Vector3.RotateTowards(transform.forward, rotateFrente, 
				                                          90 * Time.deltaTime, 0.0f);
			}
			break;
		case stateE:
			if (stateTo == stateB) {
				transform.forward = Vector3.RotateTowards(transform.forward, rotateCima, 
				                                          90 * Time.deltaTime, 0.0f);
			} else if (stateTo == stateF) {
				transform.forward = Vector3.RotateTowards(transform.forward, rotateFrente, 
				                                          90 * Time.deltaTime, 0.0f);
			} else if (stateTo == stateD) {
				transform.forward = Vector3.RotateTowards(transform.forward, rotateAtras, 
				                                          90 * Time.deltaTime, 0.0f);
			}
			break;
		case stateF:
			if (stateTo == stateC) {
				transform.forward = Vector3.RotateTowards(transform.forward, rotateCima, 
				                                          90 * Time.deltaTime, 0.0f);
			} else if (stateTo == stateE) {
				transform.forward = Vector3.RotateTowards(transform.forward, rotateAtras, 
				                                          90 * Time.deltaTime, 0.0f);
			}
			break;
		}
	}
}
