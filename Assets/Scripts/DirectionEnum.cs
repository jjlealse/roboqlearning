﻿using UnityEngine;
using System.Collections;

public enum DirectionEnum {
	Left, 
	Right,
	Up,
	Down
}
