﻿using UnityEngine;
using System.Collections;

public class QlearningScript : MonoBehaviour {

	readonly double alpha = 0.1;
	readonly double gamma = 0.9;

	// Estados A,B,C,D,E,F
	// Obs. from A nos podemos ir para B ou D
	// De C nos podemos ir apenas para C 
	// C é o estado objetivo, recompensa 100 quando B->C ou F->C
	// 
	// _______
	// |A|B|C|
	// |_____|
	// |D|E|F|
	// |_____|
	//
	static readonly int stateA = 0;
	static readonly int stateB = 1;
	static readonly int stateC = 2;
	static readonly int stateD = 3;
	static readonly int stateE = 4;
	static readonly int stateF = 5;

	const int statesCount = 6;
	static readonly int[] states = new int[]{ stateA, stateB, stateC, stateD, stateE, stateF };

	// http://en.wikipedia.org/wiki/Q-learning
	// http://people.revoledu.com/kardi/tutorial/ReinforcementLearning/Q-Learning.htm
	
	// Q(s,a)= Q(s,a) + alpha * (R(s,a) + gamma * Max(next state, all actions) - Q(s,a))
	
	// Recompensa
	static int[][] R = new int[statesCount][] {
		new int[]{0, 0, 0, 0, 0, 0},
		new int[]{0, 0, 0, 0, 0, 0},
		new int[]{0, 0, 0, 0, 0, 0},
		new int[]{0, 0, 0, 0, 0, 0},
		new int[]{0, 0, 0, 0, 0, 0},
		new int[]{0, 0, 0, 0, 0, 0}
	};
	static double[][] Q = new double[statesCount][] {
		new double[]{0, 0, 0, 0, 0, 0},
		new double[]{0, 0, 0, 0, 0, 0},
		new double[]{0, 0, 0, 0, 0, 0},
		new double[]{0, 0, 0, 0, 0, 0},
		new double[]{0, 0, 0, 0, 0, 0},
		new double[]{0, 0, 0, 0, 0, 0}
	};

	static int[] actionsFromA = new int[] { stateB, stateD };
	static int[] actionsFromB = new int[] { stateA, stateC, stateE };
	static int[] actionsFromC = new int[] { stateC };
	static int[] actionsFromD = new int[] { stateA, stateE };
	static int[] actionsFromE = new int[] { stateB, stateD, stateF };
	static int[] actionsFromF = new int[] { stateC, stateE };

	static int[][] actions = new int[][] 
	{
		actionsFromA, 
		actionsFromB, 
		actionsFromC,
		actionsFromD,
		actionsFromE,
		actionsFromF
	};

	static string[] stateNames = new string[]{ "A", "B", "C", "D", "E", "F"};

	// Use this for initialization
	void Start () {
		Init ();
		Run ();
		showPolicy ();
	}

	private void Init() 
	{
		R [stateB][stateC] = 100;
		R [stateF][stateC] = 100;
	}

	private void Run() {
		/*
        1. Set parameter , and environment reward matrix R 
        2. Initialize matrix Q as zero matrix 
        3. For each episode: Select random initial state 
           Do while not reach goal state o 
               Select one among all possible actions for the current state o 
               Using this possible action, consider to go to the next state o 
               Get maximum Q value of this next state based on all possible actions o 
               Compute o Set the next state as the current state
        */

		//Treino
		for (int i = 0; i < 1000; i++) {
			// Seleciona aleatoriamente o estado inicial.
			int state = Random.Range(0, statesCount);
			while (state != stateC) {

				// Escolhe uma entre todas as acoes possiveis para o estado atual.
				int[] actionsFromState = actions[state];

				// A estrategia de selecao é aleatoria.
				int index = Random.Range(0, actionsFromState.Length);
				int action = actionsFromState[index];

				// O resultado da ação é definida como determinista, probabilidade de transição é 1.
				int nextState = action;
				
				double q = getQ(state, action);
				double maxQ = valueMaxQ(nextState);
				int r = getR(state, action);
				
				double value = q + alpha * (r + gamma * maxQ - q);
				setQ(state, action, value);
				
				// Atribui o nextState como o estado corrente.
				state = nextState;
			}
		}

	}

	// Retorna a politica a partir de um estado.
	public int getPolicy(int state) {
		int[] actionsFromState = actions[state];
		double maxValue = double.MinValue;
		
		// Se não for encontrado, o padrao é ele mesmo.
		int policyGotoState = state;
		for (int i = 0; i < actionsFromState.Length; i++) {
			int nextState = actionsFromState[i];
			double value = Q[state][nextState];
			
			if (value > maxValue) {
				maxValue = value;
				policyGotoState = nextState;
			}
		}
		return policyGotoState;
	}

	private double valueMaxQ(int s) {
		int[] actionsFromState = actions[s];
		double maxValue = double.MinValue;
		for (int i = 0; i < actionsFromState.Length; i++) {
			int nextState = actionsFromState[i];
			double value = Q[s][nextState];
			
			if (value > maxValue) {
				maxValue = value;
			}
		}
		return maxValue;
	}

	private double getQ(int s, int a) {
		return Q[s][a];
	}
	
	private void setQ(int s, int a, double value) {
		Q[s][a] = value;
	}
	
	private int getR(int s, int a) {
		return R[s][a];
	}

	// Politica é maxQ(states)
	void showPolicy() {
		Debug.Log("Politica Otima: \n");
		for (int i = 0; i < states.Length; i++) {
			int from = states[i];
			int to = getPolicy(from);
			Debug.Log("De " + stateNames[from] + " para " + stateNames[to]);
		}
	}
}
