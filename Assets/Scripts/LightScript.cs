﻿using UnityEngine;
using System.Collections;

public class LightScript : MonoBehaviour {

	public Transform target;

	public float speed;

	public Light light;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetAxis("Vertical") < 0) {
			light.intensity = 8;
		}

		light.intensity = Mathf.Lerp(light.intensity, 1.7f, Time.deltaTime * speed);
		Vector3 wantedPosition = new Vector3(transform.position.x,
		                                     transform.position.y,
		                                     target.position.z);

		transform.position = Vector3.Lerp(transform.position, wantedPosition,
		                                  Time.deltaTime * speed);
	}
}
